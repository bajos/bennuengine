//
// Created by patryk on 14.02.16.
//

#ifndef BENNUPROJECT_BETEXREADER_HPP
#define BENNUPROJECT_BETEXREADER_HPP

#include <string>
#include <vector>
#include <memory>

#include <bennu/util/betex/Header.hpp>
#include <bennu/util/betex/TexImageHeader.hpp>
#include <bennu/util/Endian.hpp>

namespace Bennu
{
	namespace Betex
	{
		class Reader
		{

		public:
			struct Data
			{
				Header * header;
				// Arrays of pointers that pointing to data in rawData;
				std::unique_ptr<TexImageHeader * []> texImageHeader;
				std::unique_ptr<void * []> texImageData;

				// All data
				std::unique_ptr<uint8_t[]> rawData;
				uint64_t rawDataLength;
			};

			bool readFromFile(const std::string & fileName, Data & outData);

		private:
			bool matchData(Data & outData);

			bool matchHeader(Data & outData);

			bool matchTexImageHeaders(Data & outData);
		};
	}
}

#endif //BENNUPROJECT_BETEXREADER_HPP
