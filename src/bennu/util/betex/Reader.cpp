//
// Created by patryk on 14.02.16.
//

#include "Reader.hpp"

#include <QString>
#include <QFile>

namespace Bennu
{
	namespace Betex
	{
		bool Reader::readFromFile(const std::string & fileName, Data & outData)
		{
			QFile file(QString::fromStdString(fileName));

			if (!file.open(QIODevice::ReadOnly))
				return false;

			auto fileSize = file.size();
			outData.rawDataLength = (uint64_t) fileSize;

			outData.rawData.reset(new uint8_t[fileSize]);

			file.read((char *) outData.rawData.get(), fileSize);

			matchData(outData);

			file.close();
			return true;
		}

		bool Reader::matchData(Data & outData)
		{
			if (!outData.rawData)
				return false;

			matchHeader(outData);
			matchTexImageHeaders(outData);

			return true;
		}

		bool Reader::matchHeader(Data & outData)
		{
			if (outData.rawDataLength >= BETEX_HEADER_SIZE)
			{
				// Bind pointer to header equals to begin of file

				Header * header = (Header *) outData.rawData.get();
				header->toEndian(Endian::Type::UNKNOWN);
				outData.header = header;
			}
			else
				return false;

			return true;
		}

		bool Reader::matchTexImageHeaders(Data & outData)
		{    // TODo: Wyjscie poza zakres danych
			uint32_t texImagesNum = outData.header->texImagesNum;

			outData.texImageHeader.reset(new TexImageHeader * [texImagesNum]);
			outData.texImageData.reset(new void * [texImagesNum]);

			/* Pointer to begin of tex headers and tex data in rawData
			 * It can't be deleted because owner of the data is outData.rawData
			 */
			void * texImageHeaderPtr = outData.rawData.get() + BETEX_HEADER_SIZE;
			void * texImageDataPtr = texImageHeaderPtr + (BETEX_TEXIMAGEHEADER_SIZE * texImagesNum);

			uint32_t texImageHeaderPtrOffset = 0;
			uint32_t texImageDataPtrOffset = 0;

			for (uint32_t it = 0; it < texImagesNum; ++it)
			{
				TexImageHeader * texImageHeader = (TexImageHeader *) (texImageHeaderPtr + texImageHeaderPtrOffset);
				texImageHeader->toEndian(outData.header->getEndian(), Endian::Type::UNKNOWN);

				// Data is endian-less
				void * texImageData = texImageDataPtr + texImageDataPtrOffset;

				outData.texImageHeader[it] = texImageHeader;
				outData.texImageData[it] = texImageData;

				texImageHeaderPtrOffset += BETEX_TEXIMAGEHEADER_SIZE;
				texImageDataPtrOffset += texImageHeader->imageSize;
			}

			return true;
		}
	}
}
