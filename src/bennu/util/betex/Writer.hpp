//
// Created by patryk on 17.02.16.
//

#ifndef BENNUENGINEPROJECT_WRITER_HPP
#define BENNUENGINEPROJECT_WRITER_HPP

#include <string>
#include <vector>

#include <bennu/util/betex/Header.hpp>
#include <bennu/util/betex/TexImageHeader.hpp>
#include <bennu/util/Endian.hpp>

namespace Bennu
{
	namespace Betex
	{
		class Writer
		{
		public:
			struct Data
			{
				Endian::Type outEndian;
				Header header;

				std::vector<TexImageHeader> texImageHeader;
				std::vector<void *> texImageData;
			};

			bool write(const std::string & fileName, const Data & inData);

		private:
			uint32_t calculateOutSize(const Data & writeData);
		};
	}
}

#endif //BENNUENGINEPROJECT_WRITER_HPP
