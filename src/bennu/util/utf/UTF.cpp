#include "UTF.hpp"

void UTF::decodeUTF8(const uint8_t * inData, uint32_t * outData)
{
    // Pobranie ilosci bajtow w UTF8
    uint32_t inputBytesNum = getFullUTFLen(UTF8, inData);

    // Jesli UTF8 jest tylko kodem ASCII (0-127)
    if(inputBytesNum == 1)
    {
        *outData = *inData;
    }
    else // Jesli jest to znak z Unicode
    {
        // Wpisanie glownego elementu
        uint32_t unicode = 0;
        uint8_t firstElem = inData[0];
        firstElem <<= inputBytesNum;
        firstElem >>= inputBytesNum;
        unicode = firstElem;

        // Dopisanie dodatkowych elementow
        for (int it = 1; it < inputBytesNum; ++it) {
            // Zrobienie miejsca na szesc bitow z kawalkow
            unicode <<= 6;
            // Wbisanie kawalka bez dodatkowych bitow 6-7
            unicode |= (inData[it] & 0b00111111);
        }

        // Zapisanie wyniku przez dereferencje
        *outData = unicode;
    }
}

void UTF::decodeUTF16(const uint16_t * inData, uint32_t * outData)
{
    // Pobranie liczby bajtow
    uint32_t inputBytesNum = getFullUTFLen(UTF16, inData);

    if(inputBytesNum == 2)
    {
        *outData = *inData;
        return;
    }
    else
    {
        uint16_t first = inData[0], second = inData[1];

        first -= 0xD800;
        second -= 0xDC00;

        uint32_t unicode = 0;
        // Wstawienie pierwszej polowki (10bit)
        unicode |= first;
        // Przesuniecie w lewo o 10bit i wstawienie na poczatek drugiej polowki
        unicode = ((unicode << 10) | second);
        // Dodanie 0x10000
        unicode += 0x10000;

        // Zapisanie wyniku przez dereferencje wskaznika
        *outData = unicode;
    }
}

void UTF::decodeUTF32(const uint32_t * inData, uint32_t * outData)
{
    *outData = *inData;
}

void UTF::decodeUTF(UTFType inUTF, const void * inData, uint32_t * outData)
{
    switch(inUTF)
    {
        case UTF8:
            decodeUTF8((uint8_t *) inData, outData);
            break;
        case UTF16:
            decodeUTF16((uint16_t *) inData, outData);
            break;
        case UTF32:
            decodeUTF32((uint32_t *) inData, outData);
            break;
        default:
            break;
    }
}

void UTF::encodeUTF8(uint32_t inData, uint8_t * outData)
{
    // Obliczenie ilosci bajtow wynikowych
    uint32_t utf8BytesNum = getUTFLenFromUni(UTF8, inData);

    // Jesli to kod ASCII
    if(utf8BytesNum == 1)
    {
        outData[0] = (uint8_t) inData;
    }

    // Uzupelnienie bajtow 0b10xxxxxx
    uint32_t tempUnicode = inData;
    for(uint8_t it = (uint8_t)(utf8BytesNum - 1); it > 0; --it)
    {
        uint8_t byte = (uint8_t)tempUnicode;
        byte &= 0b00111111;
        byte |= 0b10000000;

        outData[it] = byte;
        tempUnicode >>= 6;
    }

    /* Uzlupelnienie pierwszego bajtu x*n0*m
     * b0: 0b11111111
     * b0: 0b00000111|11111
     * b0: 0b11100000
     * b0: 0b1110xxxx
     */
    uint8_t byte0 = 255;
    uint8_t byte0Shift = (uint8_t)(8 - utf8BytesNum);
    byte0 >>= byte0Shift;
    byte0 <<= byte0Shift;
    byte0 |= tempUnicode;
    outData[0] = byte0;
}

void UTF::encodeUTF16(uint32_t inData, uint16_t * outData)
{
    uint32_t utf16BytesNum = getUTFLenFromUni(UTF16, inData);

    if (utf16BytesNum == 2) {
        outData[0] = (uint16_t)inData;
        return;
    }
    else if(utf16BytesNum == 4)
    {
        uint32_t unicode = inData;
        // Odjecie od unicode liczby 0x10000 aby powstala liczba 20-bit
        unicode -= 0x10000;

        uint16_t first, second;

        // Podzielenie na bity od 0-9
        second = (uint16_t)unicode;
        second &= 0b1111111111;

        // I bity od 10-19
        unicode >>= 10;
        first = (uint16_t)unicode;

        // Do pierwszej polowy dodac 0xD800, a do drugiej 0xDC00
        first += 0xD800;
        second += 0xDC00;

        // Przypisac powstale liczby do outputu
        outData[0] = first;
        outData[1] = second;
    }
}

void UTF::encodeUTF32(uint32_t inData, uint32_t * outData)
{
    *outData = inData;
}

void UTF::encodeUTF(uint32_t inData, UTFType outUTF, void * outData)
{
    switch(outUTF)
    {
        case UTF8:
            encodeUTF8(inData, (uint8_t *) outData);
            break;
        case UTF16:
            encodeUTF16(inData, (uint16_t *) outData);
            break;
        case UTF32:
            *(uint32_t*) outData = inData;
            break;
        default:
            break;
    }
}

uint32_t UTF::getFullUTFLen(UTFType inUTF, const void * inData)
{
    if(inUTF == UTF8) {
        uint8_t byte = *(uint8_t*) inData;

        // Jesli UTF8 jest czystym kodem ASCII
        if (byte <= 127)
        {
            return 1;
        }
        else // Jesli sklada sie z kilku bajtow
        {
            uint8_t counter = 0, mask = 0b10000000;
            while (byte & mask)
            {
                ++counter;
                mask >>= 1;
            }

            // Zwraca liczbe jesynek na koncu bajtu
            return counter;
        }
    }
    else if(inUTF == UTF16)
    {
        uint16_t pair = *(uint16_t*) inData;

        // Jesli znajduje sie w zakresie zarezerwowanym do kodowania zwraca 4
        if((pair >= 0xD800) && (pair <= 0xDBFF))
        {
            return 4;
        }
        else
        {
            return 2;
        }
    }
    else if(inUTF == UTF32)
    {
        return 4;
    }

    return 1;
}

uint32_t UTF::getUTFLenFromUni(UTFType outUTF, uint32_t unicode)
{
    if(outUTF == UTF8)
    {
        // Jesli to kod ASCII
        if(unicode <= 127)
        {
            return 1;
        }
        else // Jesli to kod unicode
        {
            // Obliczenie liczby bitow z unicode
            uint32_t tempUni = unicode;
            uint32_t uniBitsNum = 0;
            while (tempUni)
            {
                tempUni >>= 1;
                ++uniBitsNum;
            }

            /* Liczba bitow z unicode w pierwszym bajcie to reszta z dzielenia przez 6
             * Po odjeciu od siedmiu liczby bitow w pierwszym bajcie otrzymamy liczbe bajtow
             * b0: 0b1110xxxx
             * Bits number (from unicode) in b0: 4
             * Bytes number: 7 - 4 = 3
             */
            uint32_t additionalUTF8Bytes = (uint32_t) (uniBitsNum / 6);
            uint32_t UTF8BytesNum = additionalUTF8Bytes + 1;

            return UTF8BytesNum;
        }
    }
    else if(outUTF == UTF16)
    {
        if(((unicode >= 0xD800) && (unicode <= 0xDBFF)) || (unicode >= 0xFFFF))
        {
            return 4;
        }
        else
        {
            return 2;
        }
    }
    else if(outUTF == UTF32)
    {
        return 4;
    }

    return 1;
}

uint32_t UTF::getUniLenFromUTFStr(UTFType inUTF, const void * inData)
{
    uint32_t utfStrRemained = getUTFStrLen(inUTF, inData);
    uint32_t utfStrPtr = 0;
    uint32_t uniStrLen = 0;
    uint32_t utfFullLen = 0;

    /**
     * Do póki będą dane do przetworzenia i nie wyjdą poza zakres
     * tablicy bedzie wykonywana pętla
     */
    while((utfFullLen <= utfStrRemained) && (utfStrRemained > 0))
    {
        // Oblicza cala dlugosc znaku utf
        utfFullLen = getFullUTFLen(inUTF, inData+utfStrPtr);

        // Inkrementuje licznik znakow unicode
        uniStrLen += 4;

        // Przesówa wskaznik znaku w ciagu
        utfStrPtr += utfFullLen;
        utfStrRemained -= utfFullLen;
    }

    return uniStrLen;
}

uint32_t UTF::getUTFLenFromUniStr(UTFType outUTF, const void * inData)
{
    uint32_t utfStrLen = 0;
    uint32_t uniStrRemained = getUTFStrLen(UTF32, inData);
    uint32_t uniStrPtr = 0;

    //Dopuki nie wykroczy poza zakres tablicy i będą dane do przeczytania > 0
    while ((4 <= uniStrRemained) && (uniStrRemained > 0))
    {
        utfStrLen += getUTFLenFromUni(outUTF, *(uint32_t *) (inData + uniStrPtr));

        uniStrPtr += 4;
        uniStrRemained -= 4;
    }

    return utfStrLen;
}

uint32_t UTF::getUTFStrLen(UTFType inUTF, const void * inData)
{
    // Enum utf mowi ile bajtow ma code unit
    uint8_t codeUnitLen = (uint8_t)inUTF;
    uint32_t strIt = 0;
    uint32_t strLen = 0;

    if(inUTF == UTF8)
    {
        uint8_t * ptr = (uint8_t*)inData;
        while (ptr[strIt] != 0)
        {
            strLen += codeUnitLen;
            ++strIt;
        }
    }
    else if(inUTF == UTF16)
    {
        uint16_t * ptr = (uint16_t*)inData;
        while (ptr[strIt] != 0)
        {
            strLen += codeUnitLen;
            ++strIt;
        }
    }
    else if(inUTF == UTF32)
    {
        uint32_t * ptr = (uint32_t*)inData;
        while (ptr[strIt] != 0)
        {
            strLen += codeUnitLen;
            ++strIt;
        }
    }
    else
    {
        return 0;
    }

    return strLen;
}

uint32_t UTF::getCharsNum(UTF::UTFType inUTF, const void * inData)
{
    uint32_t uniLen = getUniLenFromUTFStr(inUTF, inData);

    return (uniLen / 4);
}

void UTF::decodeUTFStr(UTF::UTFType inUTF, const void * inData, uint32_t * outData)
{
    uint32_t utfStrRemained = getUTFStrLen(inUTF, inData);
    uint32_t utfStrPtr = 0;
    uint32_t uniStrPtr = 0;
    uint32_t utfFullLen = 0;

    /**
     * Do póki będą dane do przetworzenia i nie wyjdą poza zakres
     * tablicy bedzie wykonywana pętla
     */
    while ((utfFullLen <= utfStrRemained) && (utfStrRemained > 0))
    {
        // Oblicza cala dlugosc znaku utf
        utfFullLen = getFullUTFLen(inUTF, inData + utfStrPtr);

        // Dekoduje znak
        decodeUTF(inUTF, inData + utfStrPtr, outData + uniStrPtr);

        // Przesówa wskaznik znaku w ciagu Unicode i UTF
        uniStrPtr += 1;
        utfStrPtr += utfFullLen;

        // Zmniejsza dlugosc pozostaluch znakow o pelna dlugosc znaku UTF
        utfStrRemained -= utfFullLen;
    }
}

void UTF::encodeUTFStr(const uint32_t * inData, UTF::UTFType outUTF, void * outData)
{
    uint32_t utfStrPtr = 0;
    uint32_t utfCharLen = 0;

    uint32_t uniStrRemained = getUTFStrLen(UTF32, inData);
    uint32_t uniStrPtr = 0;

    /**
     * Do póki będą dane do przetworzenia i nie wyjdą poza zakres
     * tablicy bedzie wykonywana pętla
     */
    while ((4 <= uniStrRemained) && (uniStrRemained > 0))
    {
        // Dekoduje znak
        encodeUTF(*(inData + uniStrPtr), outUTF, outData + utfStrPtr);

        // Oblicza cala dlugosc znaku utf
        utfCharLen = getUTFLenFromUni(outUTF, *(inData + uniStrPtr));

        // Przesówa wskaznik znaku w ciagu Unicode i UTF
        uniStrPtr += 1;
        utfStrPtr += utfCharLen;

        // Zmniejsza dlugosc pozostalych znakow o pelna dlugosc znaku UTF
        uniStrRemained -= 4;
    }
}
