#ifndef UTFTRANSLATOR_HPP_
#define UTFTRANSLATOR_HPP_

#include <math.h>
#include <stdint.h>
#include <cstring>

#include <bennu/util/Endian.hpp>

/**
 * @brief Klasa obsługująca kodowanie/dekodowanie UTF
 *
 * Klasa operuje na znakach UTF w endianess odpowiadającemu isLE procesora
 */
class UTF
{
public:
	enum UTFType { UNKNOWN=0, UTF8=1, UTF16=2, UTF32=4 };
public:
    // TODO Zrobić funkcje przetwarzającą UTF na Endian procesora

    static void decodeUTF8(const uint8_t * inData, uint32_t * outData);
    static void decodeUTF16(const uint16_t * inData, uint32_t * outData);
	static void decodeUTF32(const uint32_t * inData, uint32_t * outData);
    static void decodeUTF(UTFType inUTF, const void * inData, uint32_t * outData);

    static void encodeUTF8(uint32_t inData, uint8_t * outData);
    static void encodeUTF16(uint32_t inData, uint16_t * outData);
	static void encodeUTF32(uint32_t inData, uint32_t * outData);
    static void encodeUTF(uint32_t inData, UTFType outUTF, void * outData);

	/**
	 * @brief Zwraca liczbe bajtów UTF na podstawie pierwszej pary/bajtu
	 *
	 * @param inEnc Kodowanie wejściowych danych
	 * @param inData Dane wejściowe
	 * @return Liczba bajtów składająca się na zakodowany code point
	 */
	static uint32_t getFullUTFLen(UTFType inUTF, const void * inData);

	/**
	 * @brief Zwraca długośc code point zakodowanego w UTF
	 *
	 * @param outEnc Kodowanie wyjściowe
	 * @param unicode Kod znaku Unicode
	 * @return Długość zakodowanego code point w podanym kodowaniu
	 */
	static uint32_t getUTFLenFromUni(UTFType outUTF, uint32_t unicode);

	/**
	 * @brief Zwraca liczbe bajtów po przekonwertowaniu tekstu UTF na Unicode
	 *
	 * @param inEnc Kodowanie wejściowe
	 * @param num Długość tablicy inData
	 * @param inData Dane wejściowe
	 */
	static uint32_t getUniLenFromUTFStr(UTFType inUTF, const void * inData);

	/**
	 * @brief Zwraca liczbe bajtów po zakodowaniu Unicode do określonego UTF
	 *
	 * @param outEnc Kodowanie wyjściowe
	 * @param num Długość tablicy inData
	 * @param inData Dane wejściowe
	 */
	static uint32_t getUTFLenFromUniStr(UTFType outUTF, const void * inData);

	/**
	 * @brief Oblicza długość w bajtach ciągu UTF zakończonych code point U+0000
	 *
	 * @param inEnc Kodowanie wejściowe
	 * @param inData Dane wejściowe
	 * @return Liczba bajtów w ciągu znaków UTF
	 */
	static uint32_t getUTFStrLen(UTFType inUTF, const void * inData);

    /**
     * @brief Oblicza liczbe znaków w ciągu UTF
     *
     * @param inUTF Kodowanie wejściowe
     * @param inData Dane wejściowe
     */
	static uint32_t getCharsNum(UTFType inUTF, const void * inData);

	static void decodeUTFStr(UTFType inUTF, const void * inData, uint32_t * outData);

	static void encodeUTFStr(const uint32_t * inData, UTFType outUTF, void * outData);
};

#endif /* UTFTRANSLATOR_HPP_ */
