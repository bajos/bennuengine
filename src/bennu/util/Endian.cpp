//
// Created by patryk on 28.09.15.
//

#include <bits/algorithmfwd.h>
#include "Endian.hpp"

namespace Bennu
{
	Endian::Endian()
	{
		uint16_t val = 1;
		uint8_t * ptr = (uint8_t *) &val;

		if (ptr[0] == 1) cpuEndian = LE;
		else cpuEndian = BE;
	}

	void Endian::toLocal(Type inEndian, uint32_t inDataLen, void * inData)
	{
		if ((cpuEndian != inEndian) && (inEndian != UNKNOWN)) swapBytesOrder(inDataLen, inData);
	}

	void Endian::toEndian(Type inEndian, uint32_t inDataLen, void * inData,
						  Type outEndian)
	{
		if ((inEndian != outEndian) && (outEndian != UNKNOWN) && (inEndian != UNKNOWN))
			swapBytesOrder(inDataLen, inData);
	}

	void Endian::swapBytesOrder(uint32_t inDataLen, void * inData)
	{
		if (inData && (inDataLen > 1))
		{
			uint64_t halfIt = inDataLen / 2;
			uint64_t reverseIt = inDataLen - 1;
			uint8_t * ptrData = (uint8_t *) inData;

			for (uint64_t it = 0; it < halfIt; ++it, --reverseIt)
			{
				std::swap(ptrData[it], ptrData[reverseIt]);
			}
		}
	}

	Endian::Type Endian::getLocalEndian()
	{
		return cpuEndian;
	}
}
