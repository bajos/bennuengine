//
// Created by patryk on 28.09.15.
//

#ifndef BENNU_UTIL_ENDIAN_HPP
#define BENNU_UTIL_ENDIAN_HPP

#include <cstdint>

#define CPU_ENDIANNESS Endian::getInstance().getCpuEndianness()

namespace Bennu
{
	class Endian
	{
	public:
		/**
		 * @brief Okresla kolejnosc bajtow
		 */
		enum Type
		{
			UNKNOWN,    ///< Jesli kodowanie jest nieznane
			BE,         ///< Little Endian
			LE          ///< Big Endian
		};

		Endian();

		/**
		 * @brief Konwertuje liczbe do isLE CPU
		 *
		 * @param inEndian Wejsciowe isLE
		 * @param inDataLen Długość danych w bajtach
		 * @param inData Wskaznik na dane wejsciowe
		 */
		void toLocal(Type inEndian, uint32_t inDataLen, void * inData);

		/**
		 * @brief Konwertuje z okreslonego isLE do innego
		 *
		 * @param inEndian Wejściowe isLE
		 * @param inDataLen Długość danych w bajtach
		 * @param inData Wskaźnik na dane wejściowe
		 * @param outEndian Wyjściowe isLE
		 */
		void toEndian(Type inEndian, uint32_t inDataLen, void * inData,
					  Type outEndian);

		/**
		 * @brief Odwraca kolejność bajtów
		 *
		 * @param inDataLen Długość danych w bajtach
		 * @param inData Wskaźnik na dane wejściowe
		 */
		void swapBytesOrder(uint32_t inDataLen, void * inData);

		/**
		 * @brief Zwraca isLE procesora.
		 *
		 * @return Endian procesora
		 */
		Type getLocalEndian();

	private:
		Type cpuEndian;
	};
}

#endif //BENNU_ENDIANNESS_HPP
