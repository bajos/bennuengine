//
// Created by patryk on 12.03.16.
//

#ifndef BENNUENGINEPROJECT_APPLICATION_HPP
#define BENNUENGINEPROJECT_APPLICATION_HPP

#include <QCoreApplication>
#include <QCommandLineParser>
#include <bennu/tools/betextool/conf/Configuration.hpp>

namespace Bennu
{
	namespace Betextool
	{
		class Application
		{
			QCoreApplication coreApplication;
			QCommandLineParser cmdParser;
			Configuration configuration;

			void parseCmd();

			void parseConfig();

		public:
			Application(int argc, char * argv[]);
		};
	}
}

#endif //BENNUENGINEPROJECT_APPLICATION_HPP
