//
// Created by patryk on 09.03.16.
//

#ifndef BENNUPROJECT_CONFROOT_HPP
#define BENNUPROJECT_CONFROOT_HPP

#include <bennu/tools/betextool/conf/GlBindTexture.hpp>
#include <bennu/tools/betextool/conf/GlTexImage.hpp>
#include <bennu/tools/betextool/conf/GlTexParameter.hpp>
#include <bennu/tools/betextool/conf/LayersNamePatterns.hpp>

class QJsonObject;

namespace Bennu
{
	namespace Betextool
	{
		class Configuration
		{
		private:
			bool compressed;
			GlBindTexture glBindTexture;
			GlTexImage glTexImage;
			GlTexParameter glTexParameter;
			LayersNamePatterns layersNamePatterns;
		public:
			bool fromJsonObject(QJsonObject const & jsonObj);

			bool isCompressed() const
			{ return compressed; }

			const GlBindTexture & getGlBindTexture() const
			{ return glBindTexture; }

			const GlTexImage & getGlTexImage() const
			{ return glTexImage; }

			const GlTexParameter & getGlTexParameter() const
			{ return glTexParameter; }

			const LayersNamePatterns & getLayersNamePatterns() const
			{ return layersNamePatterns; }
		};
	}
}

#endif //BENNUPROJECT_CONFROOT_HPP
