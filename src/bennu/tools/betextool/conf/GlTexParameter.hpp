//
// Created by patryk on 12.03.16.
//

#ifndef BENNUPROJECT_GLTEXPARAMETER_HPP
#define BENNUPROJECT_GLTEXPARAMETER_HPP

#include <GL/gl.h>

class QJsonObject;


namespace Bennu
{
	namespace Betextool
	{
		class GlTexParameter
		{
		public:
			bool fromJsonObject(const QJsonObject & jsonObj);
		private:
			void parseMinFilter(const QJsonObject & jsonObj);

			void parseMagFilter(const QJsonObject & jsonObj);

			void parseWrapping(const QJsonObject & jsonObj);

			GLint glTextureLodBias;
			GLenum glTextureMinFilter;
			GLenum glTextureMagFilter;
			GLint glTextureMinLod;
			GLint glTextureMaxLod;
			GLint glTextureMaxLevel;
			GLenum glTextureWrapS;
			GLenum glTextureWrapT;
			GLenum glTextureWrapR;
		};
	}
}

#endif //BENNUPROJECT_GLTEXPARAMETER_HPP
