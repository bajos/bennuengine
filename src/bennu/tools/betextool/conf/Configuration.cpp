//
// Created by patryk on 09.03.16.
//

#include "Configuration.hpp"

#include <QJsonObject>

namespace Bennu
{
	namespace Betextool
	{
		bool Configuration::fromJsonObject(QJsonObject const & jsonObj)
		{
			compressed = jsonObj["compressed"].toBool(true);

			glTexImage.fromJsonObject(jsonObj["glTexImage"].toObject());
			glBindTexture.fromJsonObject(jsonObj["glBindTexture"].toObject());
			glTexParameter.fromJsonObject(jsonObj["glTexParameter"].toObject());
			layersNamePatterns.fromJsonObject(jsonObj["layersNamePatterns"].toObject());

			return false;
		}
	}
}