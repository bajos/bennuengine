//
// Created by patryk on 09.03.16.
//

#include "GlBindTexture.hpp"

#include <QJsonObject>
#include <QMap>

namespace Bennu
{
	namespace Betextool
	{
		bool GlBindTexture::fromJsonObject(const QJsonObject & jsonObj)
		{
			static QMap<QString, GLenum> targetMap{
					{"GL_TEXTURE_1D",       GL_TEXTURE_1D},
					{"GL_TEXTURE_2D",       GL_TEXTURE_2D},
					{"GL_TEXTURE_3D",       GL_TEXTURE_3D},
					{"GL_TEXTURE_CUBE_MAP", GL_TEXTURE_CUBE_MAP}
			};

			// Default is 2D texture
			target = targetMap[jsonObj["target"].toString("GL_TEXTURE_2D")];

			return true;
		}
	}
}