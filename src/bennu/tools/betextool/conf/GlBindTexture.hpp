//
// Created by patryk on 09.03.16.
//

#ifndef BENNUPROJECT_GLBINDTEXTURE_HPP
#define BENNUPROJECT_GLBINDTEXTURE_HPP

#include <GL/gl.h>

class QJsonObject;

namespace Bennu
{
	namespace Betextool
	{
		class GlBindTexture
		{
		private:
			GLenum target;
		public:
			bool fromJsonObject(QJsonObject const & jsonObj);

			GLenum getTarget() const
			{ return target; }
		};
	}
}


#endif //BENNUPROJECT_GLBINDTEXTURE_HPP
