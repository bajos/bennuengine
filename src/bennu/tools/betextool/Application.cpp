//
// Created by patryk on 12.03.16.
//

#include "Application.hpp"

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

#include <bennu/tools/betextool/conv/ConverterFactory.hpp>

namespace Bennu
{
	namespace Betextool
	{
		Application::Application(int argc, char ** argv) : coreApplication(argc, argv)
		{
			coreApplication.setApplicationName("Bennutextool");
			coreApplication.setApplicationVersion("0.1");

			parseCmd();
			parseConfig();

			for (auto & file : cmdParser.positionalArguments())
			{
				std::unique_ptr converter = ConverterFactory::createConverter(file);

				converter->convert(file, configuration);
			}
		}

		void Application::parseConfig()
		{
			QFile configFile(cmdParser.value("config"));
			configFile.open(QIODevice::ReadOnly);

			QJsonParseError err;
			QJsonDocument configJson;
			configJson.fromJson(configFile.readAll(), &err);

			configFile.close();

			configuration.fromJsonObject(configJson.object());
		}

		void Application::parseCmd()
		{
			cmdParser.setApplicationDescription("Betextool converts png images to Bennu .betex format.");
			cmdParser.addHelpOption();
			cmdParser.addVersionOption();

			/*	Option -c or --config allows give custom configuration.
			 *	Positional argument files allows give file or files to conversion.
			 *	Output file name will be the same but with changed extension.
			 */
			cmdParser.addOption(QCommandLineOption({"c", "config"}, "Configuration in JSON", "config",
												   "./default-configuration.json"));
			cmdParser.addPositionalArgument("files", "List of files to convert");

			cmdParser.process(coreApplication);
		}
	}
}