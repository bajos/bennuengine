//
// Created by patryk on 13.03.16.
//

#ifndef BENNUENGINEPROJECT_CONVERTER_HPP
#define BENNUENGINEPROJECT_CONVERTER_HPP

#include <bennu/tools/betextool/conf/Configuration.hpp>

class QString;

namespace Bennu
{
	namespace Betextool
	{
		class Converter
		{
		public:
			virtual ~Converter()
			{ };
			virtual bool convert(const QString & inFile, const Configuration & configuration) = 0;
		};
	}
}


#endif //BENNUENGINEPROJECT_CONVERTER_HPP
