//
// Created by patryk on 13.03.16.
//

#ifndef BENNUENGINEPROJECT_CONVERTERFACTORY_HPP
#define BENNUENGINEPROJECT_CONVERTERFACTORY_HPP

#include <memory>
#include <bennu/tools/betextool/conv/Converter.hpp>

class QString;

namespace Bennu
{
	namespace Betextool
	{
		class ConverterFactory
		{
		public:
			static std::unique_ptr<Converter> createConverter(const QString & inFile);
		};
	}
}

#endif //BENNUENGINEPROJECT_CONVERTERFACTORY_HPP
