//
// Created by patryk on 13.03.16.
//

#include "PngConverter.hpp"

#include <FreeImage.h>
#include <iostream>

namespace Bennu
{
	namespace Betextool
	{
		bool PngConverter::convert(const QString & inFile, const Configuration & configuration)
		{
			std::string inFileStdStr = inFile.toStdString();

			// Check type by file header
			FREE_IMAGE_FORMAT format = FreeImage_GetFileType(inFileStdStr.c_str());
			if (format == FIF_UNKNOWN)
			{
				//Check type by file name
				format = FreeImage_GetFIFFromFilename(inFileStdStr.c_str());
				if (format == FIF_UNKNOWN)
				{
					std::cerr << "[SKIPPING] Could not find image: " << inFileStdStr << std::endl;
					return false;
				}
			}

			if (!FreeImage_FIFSupportsReading(format))
			{
				std::cerr << "[SKIPPING] FreeImage can not read this image type: " <<
				inFileStdStr << std::endl;
				return false;
			}

			FIBITMAP * bitmap = FreeImage_Load(format, inFileStdStr.c_str());

			// If loading failed
			if (!bitmap)
			{
				std::cerr << "[SKIPPING] FreeImage can not load image: " << inFileStdStr << std::endl;
				return false;
			}

			auto bpp = FreeImage_GetBPP(bitmap);
			if (bpp != 32)
			{
				FIBITMAP * bitmap32 = FreeImage_ConvertTo32Bits(bitmap);

				// Can not convert
				if (bitmap32 == NULL)
				{
					std::cerr << "[SKIPPING] FreeImage can not convert image to 32bpp: " <<
					inFileStdStr << std::endl;
					return false;
				}

				// Swap images
				FreeImage_Unload(bitmap);
				bitmap = bitmap32;
			}

			// Now we have converted and loaded image
			// TODO: Extract code to some methods
			// TODO: Compression and writing to .betex

			FreeImage_Unload(bitmap);
			return true;
		}
	}
}