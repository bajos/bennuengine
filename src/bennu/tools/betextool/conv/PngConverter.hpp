//
// Created by patryk on 13.03.16.
//

#ifndef BENNUENGINEPROJECT_PNGCONVERTER_HPP
#define BENNUENGINEPROJECT_PNGCONVERTER_HPP

#include <bennu/tools/betextool/conv/Converter.hpp>

namespace Bennu
{
	namespace Betextool
	{
		class PngConverter : public Converter
		{
		public:
			virtual bool convert(const QString & inFile, const Configuration & configuration) override;
		};
	}
}

#endif //BENNUENGINEPROJECT_PNGCONVERTER_HPP
