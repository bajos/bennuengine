//
// Created by patryk on 13.03.16.
//

#include "ConverterFactory.hpp"

#include <memory>
#include <bennu/tools/betextool/conv/PngConverter.hpp>

namespace Bennu
{
	namespace Betextool
	{
		std::unique_ptr<Converter> ConverterFactory::createConverter(const QString & inFile)
		{
			if (inFile.endsWith(".png"))
				return std::unique_ptr(new PngConverter());
			else
				return std::unique_ptr(nullptr);
		}
	}
}