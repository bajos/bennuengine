//
// Created by patryk on 13.02.16.
//

#include <bennu/tools/betextool/Application.hpp>

int main(int argc, char * argv[])
{
	Bennu::Betextool::Application app(argc, argv);

	return 0;
}
