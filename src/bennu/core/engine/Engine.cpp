#include <bennu/core/engine/Engine.hpp>

Bennu::Engine::Engine() : resMgr(NULL), view(this)
{
    resMgr = new MapResourceManager();
}

Bennu::Engine::~Engine()
{
    delete resMgr;
}

int Bennu::Engine::main()
{
    std::cout << "Hello world!!!" << std::endl;

    Logger & log = Logger::getInstance();
    log.createLog("logs/");

    log.log("Hello world log!");

	view.createWindow();

    return 0;
}

Bennu::ResourceManagerInterface * Bennu::Engine::getResMgr()
{
    return resMgr;
}
