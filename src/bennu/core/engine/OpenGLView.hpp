//
// Created by patryk on 26.10.15.
//

#ifndef BENNUENGINE_WINDOW_HPP
#define BENNUENGINE_WINDOW_HPP

#include <bennu/core/resources/MapResourceManager.hpp>

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <QFile>

namespace Bennu
{
	class Engine;

	class OpenGLView
	{
	protected:
		sf::Window * wnd;
		Engine * eng;
		ResourceManagerInterface * resMgr;

		GLuint program;
	public:
		OpenGLView(Engine * srcEng);

		~OpenGLView();

		void createWindow();

		void initializeGL();

		void initializeScene();

		void mainLoop(bool & running);
	};
}

#endif //BENNUENGINE_WINDOW_HPP
