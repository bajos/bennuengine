//
// Created by patryk on 26.10.15.
//

#include <GL/glew.h>

#include "OpenGLView.hpp"
#include <bennu/core/engine/Engine.hpp>
#include <bennu/core/resources/impl/Shader.hpp>
#include <bennu/core/resources/impl/GpuProgram.hpp>

Bennu::OpenGLView::OpenGLView(Engine * srcEng) : eng(srcEng)
{
	resMgr = eng->getResMgr();
}

Bennu::OpenGLView::~OpenGLView()
{
}

void Bennu::OpenGLView::createWindow()
{
	wnd = new sf::Window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, sf::ContextSettings(32));
	wnd->setVerticalSyncEnabled(true);

	initializeGL();
	initializeScene();

	bool running = true;
	while (running)
	{
		// handle events
		sf::Event event;
		while (wnd->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				running = false;
			}
			else if (event.type == sf::Event::Resized)
			{
				glViewport(0, 0, event.size.width, event.size.height);

				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();
				gluPerspective(70, event.size.width / event.size.height, 0.1, 100);
				glMatrixMode(GL_MODELVIEW);
			}
		}

		mainLoop(running);

		// end the current frame (internally swaps the front and back buffers)
		wnd->display();
	}

	delete wnd;
}

void Bennu::OpenGLView::initializeGL()
{
	glewInit();
}

void Bennu::OpenGLView::initializeScene()
{
	resMgr = eng->getResMgr();
	if (resMgr == NULL) std::cout << "resmgr nie dziala" << std::endl;

	Shader * vs = (Shader *) resMgr->getRes("media/shaders/shader.vs");
	Shader * fs = (Shader *) resMgr->getRes("media/shaders/shader.fs");

	vs = (Shader *) resMgr->newRes("media/shaders/shader.vs", new Shader);
	fs = (Shader *) resMgr->newRes("media/shaders/shader.fs", new Shader);

	vs->loadFromFile(GL_VERTEX_SHADER);
	fs->loadFromFile(GL_FRAGMENT_SHADER);

	GpuProgram * prog = (GpuProgram *) resMgr->getRes("media/shaders/shader.gpu");
	prog = (GpuProgram *) resMgr->newRes("shader.gpu", new GpuProgram);
	prog->loadFromShaders(vs->getId(), fs->getId());
	program = prog->getId();

	GLfloat points[] = {
			0.0f, 0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			-0.5f, -0.5f, 0.0f
	};

	GLfloat colors[] = {
			1, 0, 0,
			0, 1, 0,
			0, 0, 1
	};

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint vbo[2];
	glGenBuffers(2, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 9, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 9, colors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	glViewport(0, 0, wnd->getSize().x, wnd->getSize().y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70, wnd->getSize().x / wnd->getSize().y, 0.1, 100);
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_DEPTH_TEST);
}

void Bennu::OpenGLView::mainLoop(bool & running)
{
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	//glTranslatef(0, 0, -10);
	//glRotatef(35, 0, 1, 0);
	glTranslatef(0, 0, -1);


	glUseProgram(program);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

