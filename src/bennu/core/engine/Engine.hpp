//
// Created by patryk on 26.10.15.
//

#ifndef BENNU_ENGINE_HPP
#define BENNU_ENGINE_HPP

#include <iostream>

#include <bennu/core/engine/OpenGLView.hpp>
#include <bennu/util/Logger.hpp>

namespace Bennu
{

	class Engine
	{
	protected:
		ResourceManagerInterface * resMgr;
		OpenGLView view;
	public:
		Engine();

		virtual ~Engine();

		int main();

		ResourceManagerInterface * getResMgr();
	};
}

#endif //BENNU_ENGINE_HPP
