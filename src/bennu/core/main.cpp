#include <bennu/core/engine/Engine.hpp>

#ifdef DEBUG

	#include <tests/UTFTest.hpp>
	#include <tests/EndianTest.hpp>
	#include <tests/MapResMgrTest.hpp>

#endif

int main(int argc, char * argv[])
{
#ifdef DEBUG
	UnitTest::RunAllTests();
#endif

	Bennu::Engine eng;
	return eng.main();
}
