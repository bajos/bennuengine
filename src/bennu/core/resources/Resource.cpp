#include "Resource.hpp"

namespace Bennu
{

    Resource::Resource() : ref(0), name()
    { }

    Resource::Resource(const Resource & src)
    {
        ref = src.ref;
        name = src.name;
    }

    Resource::~Resource()
    { }

    void Resource::decRef()
    {
        --ref;
    }

    int64_t Resource::getRef()
    {
        return ref;
    }

    void Resource::incRef()
    {
        ++ref;
    }

    const ResourceManagerInterface::ResourceName & Resource::getName()
    {
        return name;
    }

    void Resource::setName(const ResourceManagerInterface::ResourceName & newname)
    {
        name = newname;
    }

}