//
// Created by patryk on 10.01.16.
//

#ifndef BENNU_RESOURCEMANAGERINTERFACE_HPP
#define BENNU_RESOURCEMANAGERINTERFACE_HPP

#include <QString>

namespace Bennu
{
	class Resource;

	class ResourceManagerInterface
	{
	public:
		typedef QString ResourceName;

		virtual Resource * getRes(ResourceName const & name) = 0;

		virtual Resource * newRes(ResourceName const & name, Resource * newRes) = 0;

		virtual void deleteRes(ResourceName const & name) = 0;

		ResourceManagerInterface();

		virtual ~ResourceManagerInterface();
	};
}

#endif //BENNU_RESOURCEMANAGERINTERFACE_HPP
