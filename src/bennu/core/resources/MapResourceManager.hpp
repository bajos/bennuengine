#ifndef RESOURCEMANAGER_HPP
#define RESOURCEMANAGER_HPP

#include <bennu/core/resources/Resource.hpp>
#include <bennu/core/resources/ResourceManagerInterface.hpp>

#include <QMap>
#include <QString>

namespace Bennu
{
	class MapResourceManager : public ResourceManagerInterface
	{
	protected:
		QMap<QString, Resource *> content;
	public:

		virtual Resource * getRes(const ResourceManagerInterface::ResourceName & name);

		virtual Resource * newRes(ResourceName const & name, Resource * newRes);

		virtual void deleteRes(const ResourceManagerInterface::ResourceName & name);

		MapResourceManager();

		virtual ~MapResourceManager();
	};
}

#endif // RESOURCEMANAGER_HPP
