#ifndef RESOURCE_HPP
#define RESOURCE_HPP

#include <bennu/core/resources/ResourceManagerInterface.hpp>

namespace Bennu
{
    class Resource
    {
    private:
        int64_t ref;
        ResourceManagerInterface::ResourceName name;
    public:
        Resource();

        Resource(const Resource & src);

        virtual ~Resource();

        virtual void decRef();

        virtual int64_t getRef();

        virtual void incRef();

        virtual const ResourceManagerInterface::ResourceName & getName();

        virtual void setName(const ResourceManagerInterface::ResourceName & newname);
    };
}

#endif // RESOURCE_HPP
