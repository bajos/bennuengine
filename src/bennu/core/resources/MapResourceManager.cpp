#include "MapResourceManager.hpp"

namespace Bennu
{
	MapResourceManager::MapResourceManager() : ResourceManagerInterface()
	{
	}

	MapResourceManager::~MapResourceManager()
	{
		// Dla wszystkich obiektow jesli nie NULL usunac
		for (Resource * elem : content) if (elem) delete elem;
	}

	Resource * MapResourceManager::getRes(const ResourceManagerInterface::ResourceName & name)
	{
		// Jesli nie istnieje zasób tworzy NULL
		if (!content.contains(name))
			content[name] = NULL;

		// Pobiera zasób, jeśli nie NULL zwieksza referencje
		Resource * res = content[name];
		if (res) res->incRef();

		// Zwraca zasób
		return res;
	}

	Resource * MapResourceManager::newRes(const ResourceManagerInterface::ResourceName & name, Resource * newRes)
	{
		// Pobiera referencje do zasobu, TODO nie thread safe WARNING!!!
		Resource *& res = content[name];

		// Jesli do zasobu przypisany obiekt zwraca NULL
		if (res)
		{
			return NULL;
		}
		else
		{
			//Jesli zasób jest pusty to przypisuje nowy i zwieksza referencje
			res = newRes;
			res->setName(name);
			res->incRef();
		}

		// Zwraca zasob w postaci wskaznika, nie referencji
		return res;
	}

	void MapResourceManager::deleteRes(const ResourceManagerInterface::ResourceName & name)
	{
		// Szuka zasobu
		auto it = content.find(name);

		// Jesli znalazlo
		if (it != content.end())
		{
			// Dekrementuje referencje
			Resource * res = it.value();
			res->decRef();

			// Jesli mniejsza od 1 to usuwa zasób
			if (res->getRef() < 1)
			{
				delete res;
				content.erase(it);
			}
		}
	}
}