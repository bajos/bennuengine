//
// Created by patryk on 09.02.16.
//

#include <GL/glew.h>

#include "GpuProgram.hpp"

bool Bennu::GpuProgram::loadFromShaders(GLuint vertex, GLuint fragment)
{
	id = glCreateProgram();

	glAttachShader(id, vertex);
	glAttachShader(id, fragment);
	glLinkProgram(id);

	return true;
}

GLuint Bennu::GpuProgram::getId()
{
	return id;
}
