//
// Created by patryk on 05.02.16.
//

#include <GL/glew.h>

#include "Shader.hpp"
#include <QFile>
#include <bennu/util/Logger.hpp>

bool Bennu::Shader::loadFromFile(GLuint type)
{
	id = glCreateShader(type);

	// Jesli nie bylo bledu
	if (id)
	{
		// Wczytuje src do tablicy bajtów
		QFile file;
		file.setFileName(getName());
		file.open(QIODevice::ReadOnly);
		QByteArray shdData = file.readAll();
		file.close();

		// Pobiera wskaznik na dane
		const GLchar * shdSrcPtr = shdData.data();

		// Wysyla src i kompiluje
		glShaderSource(id, 1, &shdSrcPtr, NULL);
		glCompileShader(id);

		// Sprawdza status kompilacji
		GLint compStat = GL_FALSE;
		glGetShaderiv(id, GL_COMPILE_STATUS, &compStat);

		// Jesli status GL_FALES (znaczy blad)
		if (compStat == GL_FALSE)
		{
			// Pobiera dlugosc logu i zajmuje miejsce
			GLint logLen;
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLen);
			GLchar * log = new GLchar[logLen];

			// Pobiera log
			GLint lenRet = 0;
			glGetShaderInfoLog(id, logLen, &lenRet, log);

			// Logguje do pliku
			Logger & logFile = Logger::getInstance();
			logFile.log("ERROR: Shader compilation error at shader: \"" + getName() + "\"\n" +
						QString::fromUtf8(log, lenRet) + "\n");

			// Zwalnia pamiec i usuwa shader
			delete[] log;
			glDeleteShader(id);

			return false;
		}

		return true;
	}

	return false;
}

GLuint Bennu::Shader::getId()
{
	return id;
}
