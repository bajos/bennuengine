//
// Created by patryk on 09.02.16.
//

#ifndef BENNU_GPUPROGRAM_HPP
#define BENNU_GPUPROGRAM_HPP

#include <GL/gl.h>
#include <bennu/core/resources/Resource.hpp>

namespace Bennu
{
	class GpuProgram : public Resource
	{
	protected:
		GLuint id;
	public:
		bool loadFromShaders(GLuint vertex, GLuint fragment);

		GLuint getId();
	};
}


#endif //BENNU_GPUPROGRAM_HPP
