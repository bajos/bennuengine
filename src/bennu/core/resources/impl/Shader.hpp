//
// Created by patryk on 05.02.16.
//

#ifndef BENNU_SHADER_HPP
#define BENNU_SHADER_HPP

#include <GL/gl.h>
#include <bennu/core/resources/Resource.hpp>

namespace Bennu
{
	class Shader : public Resource
	{
	protected:
		GLuint id;
	public:
		bool loadFromFile(GLuint type);

		GLuint getId();
	};
}

#endif //BENNU_SHADER_HPP
