#ifndef BENNUENGINE_UTFTEST_HPP
#define BENNUENGINE_UTFTEST_HPP

#include <unittest++/UnitTest++.h>

#include <bennu/util/utf/UTF.hpp>

#define utf8str { 0b11100111, 0b10110101, 0b10110001,\
 0b11100100, 0b10111000, 0b10000000,\
 0b11100111, 0b10100010, 0b10111100, 0 };
#define utf16str { 0x7D71, 0x4E00, 0x78BC, 0};
#define utf32str { 0x00007D71, 0x00004E00, 0x000078BC, 0};

TEST(getUTFStrLen)
{
	uint8_t str8[] = utf8str;
	CHECK_EQUAL(9, UTF::getUTFStrLen(UTF::UTF8, str8));

	uint16_t str16[] = utf16str;
	CHECK_EQUAL(3 * 2, UTF::getUTFStrLen(UTF::UTF16, str16));

	uint32_t str32[] = utf32str;
	CHECK_EQUAL(3 * 4, UTF::getUTFStrLen(UTF::UTF32, str32));
}

TEST(getFullUTFLen)
{
	uint8_t str8[] = utf8str;
	CHECK_EQUAL(3, UTF::getFullUTFLen(UTF::UTF8, str8));

	uint16_t str16[] = utf16str;
	CHECK_EQUAL(2, UTF::getFullUTFLen(UTF::UTF16, str16));
}

TEST(getUTFLenFromUni)
{
	uint32_t uni = 0x00007D71;

	CHECK_EQUAL(3, UTF::getUTFLenFromUni(UTF::UTF8, uni));

	CHECK_EQUAL(2, UTF::getUTFLenFromUni(UTF::UTF16, uni));
}

TEST(getUniLenFromUTFStr)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;

	CHECK_EQUAL(3 * 4, UTF::getUniLenFromUTFStr(UTF::UTF8, str8));
	CHECK_EQUAL(3 * 4, UTF::getUniLenFromUTFStr(UTF::UTF16, str16));
}

TEST(getUTFLenFromUniStr)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;
	uint32_t str32[] = utf32str;

	CHECK_EQUAL(UTF::getUTFStrLen(UTF::UTF8, str8), UTF::getUTFLenFromUniStr(UTF::UTF8, str32));
	CHECK_EQUAL(UTF::getUTFStrLen(UTF::UTF16, str16), UTF::getUTFLenFromUniStr(UTF::UTF16, str32));
}

TEST(encodeUTF)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;
	uint32_t str32[] = utf32str;

	uint32_t uni = str32[0];
	uint8_t buf[8];

	UTF::encodeUTF(uni, UTF::UTF16, buf);
	CHECK_EQUAL(0, std::memcmp(buf, str16, 2));

	UTF::encodeUTF(uni, UTF::UTF8, buf);
	CHECK_EQUAL(0, std::memcmp(buf, str8, 3));
}

TEST(decodeUTF)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;
	uint32_t str32[] = utf32str;

	uint32_t uni = 0;

	UTF::decodeUTF(UTF::UTF8, str8, &uni);
	CHECK_EQUAL(0, std::memcmp(&uni, str32, 4));

	UTF::decodeUTF(UTF::UTF16, str16, &uni);
	CHECK_EQUAL(0, std::memcmp(&uni, str32, 4));
}

TEST(encodeUTFStr)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;
	uint32_t str32[] = utf32str;

	uint8_t buf[16];

	UTF::encodeUTFStr(str32, UTF::UTF8, buf);
	CHECK_EQUAL(0, std::memcmp(buf, str8, 9));

	UTF::encodeUTFStr(str32, UTF::UTF16, buf);
	CHECK_EQUAL(0, std::memcmp(buf, str16, 6));
}

TEST(decodeUTFStr)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;
	uint32_t str32[] = utf32str;

	uint8_t buf[16];

	UTF::decodeUTFStr(UTF::UTF8, str8, (uint32_t *) buf);
	CHECK_EQUAL(0, std::memcmp(buf, str32, 3 * 4));

	UTF::decodeUTFStr(UTF::UTF16, str16, (uint32_t *) buf);
	CHECK_EQUAL(0, std::memcmp(buf, str32, 3 * 4));
}

TEST(getUTFCharsNum)
{
	uint8_t str8[] = utf8str;
	uint16_t str16[] = utf16str;
	uint32_t str32[] = utf32str;

	CHECK_EQUAL(3, UTF::getCharsNum(UTF::UTF8, str8));
	CHECK_EQUAL(3, UTF::getCharsNum(UTF::UTF16, str16));
	CHECK_EQUAL(3, UTF::getCharsNum(UTF::UTF32, str32));
}

#endif //BENNUENGINE_UTFTEST_HPP
