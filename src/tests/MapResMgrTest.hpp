//
// Created by patryk on 06.02.16.
//

#ifndef BENNUENGINE_MAPRESMGRTEST_HPP
#define BENNUENGINE_MAPRESMGRTEST_HPP

#include <unittest++/UnitTest++.h>

#include <bennu/core/resources/MapResourceManager.hpp>
#include <bennu/core/resources/impl/Shader.hpp>

TEST(mapResMgrTest)
{
	Bennu::ResourceManagerInterface * resMgr = new Bennu::MapResourceManager;

	// Pobiera zasob
	auto res = resMgr->getRes("shader");

	// Jesli cos jest przypisane
	// POWINNO SIE WYKONAC!!!
	if (res == NULL)
	{
		res = resMgr->newRes("shader", new Bennu::Shader);
	}

	auto res2 = resMgr->getRes("shader");
	CHECK_EQUAL(res, res2);

	resMgr->deleteRes("shader");
	resMgr->deleteRes("shader");

	delete resMgr;
}

#endif //BENNUENGINE_MAPRESMGRTEST_HPP
