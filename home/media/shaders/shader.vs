#version 330 core
layout (location = 0) in vec3 position; // The position variable has attribute position 0
layout (location = 1) in vec3 color;	// The color variable has attribute position 1

out vec3 ourColor; // Output a color to the fragment shader

void main()
{
    gl_Position = gl_ModelViewProjectionMatrix * vec4(position, 1.0);
    //gl_Position = vec4(position, 1.0) * gl_ModelViewProjectionMatrix;
    ourColor = color; // Set ourColor to the input color we got from the vertex data
}
